#!/bin/bash

# Set mirror to switzerland
sudo apt install -y python3 python3-pip
pip3 install apt-select
apt-select --country CH

# Remove bloatware
sudo apt purge  -y  \ 
    thunderbird \
    transmission \
    libreoffice* \
    mah-jong \
    aisleriot \
    sudoku
    mines \
    python

sudo apt update
sudo apt upgrade -y


# Programming
sudo apt install -y \
    python3 python3-pip \
    git \
    build-essential \
    gcc \
    apache2 php libapache2-mod-php


#Other tools
sudo apt install -y \
    sublime \ 
    vim \
    vlc \
    keepassx \    
    ufw gufw \
    htop \
	gparted \  
    ubuntu-restricted-extras

#pip3 install tensorflow
pip3 install pyserial

# Clean up leftover
sudo apt autoclean -y && sudo apt autoremove -y


# Change owner of default html webpage
sudo chown $USER: /var/www/html/index.html


# Configure git
git config --global core.editor "vim"
#git config --global user.name "Pascal Haefliger"
#git config --global user.email p.haefliger@example.com


# Add custom aliases
echo "
# naviagtion
alias ..='cd ..'
alias ...='cd .. && cd ..'
alias ll='ls -als --color=auto'
alias lr='lr -alsR --color=auto'
alias cp='cp -iv'
alias mv='mv -iv'

#programs
alias gh='history|grep'
" >> .bash_alias

# Enable GNOME Night Light Mode
gsettings set org.gnome.settings-daemon.plugins.color night-light-enabled true




#ToDo manually

# Change keyboard shortcuts
#	CTRL + T: Open terminal

# Generate SSH key and add it to bitbucket
#	https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/#-Set-up-SSH-on-macOS-Linux